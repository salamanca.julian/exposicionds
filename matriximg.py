import turtle as tur

##
def confBasica():
    vent = tur.Screen()
    vent.bgcolor("black")
    vent.title("Laberinto agente")
    vent.setup(700, 700)
    vent.colormode(255)
    tur.tracer(0,0)
    ## imagenes set-up
    
    for i in range(7): 
        vent.addshape("img/"+str(i)+".gif")
        vent.addshape("img_bot/"+str(i)+".gif")
    


## Creacion de objetos
## casilla
class Casilla(tur.Turtle): 
    def __init__(self, estado, tipo,screen_x,screen_y,posx,posy): 
        tur.Turtle.__init__(self)
        self.tipo = tipo
        self.posx, self.poxy  = posx, posy
        self.screen_x, self.screen_y = screen_x, screen_y
        dir = "img"
        if (estado): 
            dir = "img_bot"
        
        self.shape(dir + "/" + str(tipo) + ".gif")
        self.penup()
        self.goto(self.screen_x,self.screen_y)
        self.stamp()

    def colorear(self,tipo):
        self.shape("square")
        self.color(self.colores[tipo])
        self.penup()
        self.goto(self.screen_x,self.screen_y)
        self.stamp()

def imp_amb(arch):
    file = open(arch, "r")
    maze = [[int(j) for j in i.split(" ")] for i in file]
    return maze
    
def imprimirMaze(maze): 
    print(len(maze))
    for x in range(len(maze)): 
        print(len(maze))
        for y in range(len(maze[x])): 
            print(maze[x][y])

def conf_amb(maze):
    arreglo = [0]*len(maze)
    for y in range(len(maze)):
        aux = []
        for x in range(len(maze[y])): 
            obj = maze[y][x]
            screen_x = -288 + (x * 31)
            screen_y = 288 - (y * 31)
            estado = False
            if (obj == 2): 
                estado = not estado
            aux.append(Casilla(estado,obj,screen_x,screen_y,x,y))
        arreglo[y] = aux
    
    return arreglo

## ejecucion
confBasica()
maze = imp_amb("ambiente.txt")
arregloDibujo = conf_amb(maze)
tur.done()